const csv = require("csv-parser");

const fs = require("fs");
const path = require("path");

function wonTossAndWonMatch(){
const matchesData = [];
const matchDataPath = path.join(__dirname,'../data/matches.csv')
fs.createReadStream(matchDataPath)
.pipe(csv())

.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
    const teamWons = {};
    for(let match of matchesData){
        if(match.toss_winner === match.winner){
            if(teamWons[match.winner]){
                teamWons[match.winner]++;
            }else {
                teamWons[match.winner] = 1;
            }
        }
    }
      console.log(teamWons);
      const jsonOutput = path.join(__dirname,'../public/output/wonTossAndWonMatch.json')
      fs.writeFile(jsonOutput,JSON.stringify(teamWons),(error)=>{
        if(error){
            console.log(error);
        }
       
})
})
}

module.exports = wonTossAndWonMatch;