const csv = require("csv-parser");

const fs = require("fs");

const path = require("path")

function extraRuns(){
const matchesData = [];
const matchDataPath = path.join(__dirname,'../data/matches.csv')
fs.createReadStream(matchDataPath)

.pipe(csv())

.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
    const deliveryData = [];
    const deliveryDataPath = path.join(__dirname,'../data/deliveries.csv')
    fs.createReadStream(deliveryDataPath)
    .pipe(csv())

    .on("data",(data)=>{
        deliveryData.push(data)
    })
    .on("error",(error)=>{
        console.log(error);
    })
    .on("end",()=>{
        let extraRuns = {};
       for(let match of matchesData){
        if(match.season === "2016"){
            for(let delivery of deliveryData){
                if(match.id === delivery.match_id){
                    if(extraRuns[delivery.batting_team]){
                        extraRuns[delivery.batting_team] += Number(delivery.extra_runs)
                     }else{
                        extraRuns[delivery.batting_team] = Number(delivery.extra_runs)
                     }

                    // console.log();
                }
            }
        }
       }
       console.log(extraRuns);
       const jsonOutput = path.join(__dirname,'../public/output/extraRuns.json')
       fs.writeFile(jsonOutput,JSON.stringify(extraRuns),(error)=>{
        if(error){
            console.log(error);
        }
       })
    })

    
})
}

module.exports = extraRuns;

