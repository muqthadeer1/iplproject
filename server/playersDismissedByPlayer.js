// importing csv and fs module
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");



function playersDismissedByPlayer(){
    let deliveryData = [];
  //create fs read module to read the CSV file
  const matchDataPath = path.join(__dirname,'../data/deliveries.csv');
  fs.createReadStream(matchDataPath)
    .pipe(csv())
    .on("data",(data) => {
      deliveryData.push(data);
    })
    .on("error",(error) => {
      console.log(error);
    })
    .on("end", () => {
  
        let newObject = {};
        //iterating through deliveries data
        for (let object of deliveryData) {
          let batsman = object.batsman;
          let bowler = object.bowler;
          // console.log(batsman);
          let dismissalKind = object.dismissal_kind;

          //if dismissalKind is true the storing the batsman and bowler
          if (dismissalKind) {
            if (newObject.hasOwnProperty(batsman)) {
              if (newObject[batsman].hasOwnProperty(bowler)) {
                newObject[batsman][bowler] += 1;
              } else {
                newObject[batsman][bowler] = 1;
              }
            } else {
              newObject[batsman] = {};
              newObject[batsman][bowler] = 1;
            }
          }
        }

        //getting the keys of newObject into array
        let keysArray = Object.keys(newObject);
        let valuesArray = Object.values(newObject);
        //   console.log(keysArray)
        let finalObject = {};
        //iterating the bowlers and finding who dismissed more times
        for (let key of keysArray) {
          let randomValue = 0;
          let randomKey;
          for (let innerKey in newObject[key]) {
            if (newObject[key][innerKey] > randomValue) {
              randomValue = newObject[key][innerKey];
              randomKey = innerKey;
            }
          }
          finalObject[key] = {
            bowler: randomKey,
            occurs: randomValue,
          };
        }
        // console.log(finalObject);

        // Object.keys(finalObject).sort((a,b)=>{
        //   // console.log(finalObject[a].occurs);
        //   if(finalObject[a].occurs !== finalObject[b].occurs){
        //     return finalObject[b].occurs-finalObject[a].occurs;
        //   }
        // })
        // Object.entries(finalObject).sort(([value1],[value2]) => {
        //   Object.entries(value1)
        // })
        // console.log(finalObject);

        const deliveryDataPath = path.join(__dirname,'../public/output/playersDismissedByPlayer.json')
        fs.writeFile(deliveryDataPath,JSON.stringify(finalObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      
    })

  }
  module.exports = playersDismissedByPlayer;

  playersDismissedByPlayer();
