const csv = require("csv-parser");
const path = require('path');
const fs = require("fs");

function matchesWonPerTeamPerYear(){
let matchesData = [];
const matchDataPath = path.join(__dirname,"../data/matches.csv");
const readStream = fs.createReadStream(matchDataPath)
.pipe(csv())
.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
    const matchesWon = {};
    for (let match of matchesData) {
        // matchesWon[year.season] = {};
      if (matchesWon[match.season]) {
        if (matchesWon[match.season][match.winner]) {
          matchesWon[match.season][match.winner]++;
        } else {
          matchesWon[match.season][match.winner] = 1;
        }
      } else {
        matchesWon[match.season] = {};
        matchesWon[match.season][match.winner]=1;
      }
    }
    console.log(matchesWon);

    const jsonOutput = path.join(__dirname,"../public/output/matchesWonPerTeamPerYear.json")
    fs.writeFile(jsonOutput,JSON.stringify(matchesWon),(error)=>{
      if(error){
      console.log(error);
      }
    })
}
)


}


    module.exports = matchesWonPerTeamPerYear;
    

  