const csv = require("csv-parser");
const path=require("path");
const fs = require("fs");

function matchesPerYear(){
let matchesData = [];

let matchpath=path.join(__dirname,"../data/matches.csv");

const readStream = fs.createReadStream(matchpath)
.pipe(csv())  
.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);  
})   
.on("end",()=>{
 const seasons = {};
  for(let year of matchesData){
    // if(seasons)
    if(seasons[year.season]){
        seasons[year.season]++;
    }
    else{
        seasons[year.season] = 1;
    }
    
 }

console.log(seasons);
let jsonOutput = path.join(__dirname,"../public/output/matchesPerYear.json");
fs.writeFile(jsonOutput,JSON.stringify(seasons),(error)=>
{
    if(error){
        console.log(error);
    }
})
})
}

module.exports = matchesPerYear;



