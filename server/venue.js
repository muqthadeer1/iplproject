const csv = require("csv-parser");

const fs = require("fs");

const matchData = []
const  readStream =  fs.createReadStream('../data/matches.csv')
.pipe(csv())

.on("data",(data)=>{
    matchData.push(data);
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
//  console.log(matchData);

 const citiesPlayed = matchData.reduce((accumulator,currentValue)=>{

    // console.log(currentValue.city);
    if(accumulator[currentValue.city]){
        accumulator[currentValue.city]++;

    } else {
        accumulator[currentValue.city] = 1 ;
    }
    return accumulator;
 },{})
 
 const highest = Object.keys(citiesPlayed).sort((a,b)=>{
    // console.log(citiesPlayed[a]);

    return citiesPlayed[b] - citiesPlayed[a];
 }).slice(0,1);
// console.log(`Most matches played in ${highest} that is ${citiesPlayed[highest]}`);
 console.log
})
