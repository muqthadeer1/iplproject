const csv = require("csv-parser");
const path = require("path");
const fs = require("fs");


function playerofTheMatch(){
let matchesData = [];
const matchDataPath = path.join(__dirname,"../data/matches.csv")
const readStream = fs.createReadStream(matchDataPath)
.pipe(csv())
.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{ 
    highestp = {}

    for(let match of matchesData){
        if(highestp[match.season]){
            if(highestp[match.season][match.player_of_match]){
                highestp[match.season][match.player_of_match]++;
            }else{
                highestp[match.season][match.player_of_match] = 1;
            }

        }else {
            highestp[match.season] = {}
            highestp[match.season][match.player_of_match] = 1;

        }
    }
    
    let keys=Object.keys(highestp);
    let values=Object.values(highestp);
    let finalArray=[];
    for(let object of values){
        let maxValue=0;
            let name;
        for(let bowlerName in object){
            
            if(object[bowlerName]>maxValue){
                maxValue=object[bowlerName];
                name=bowlerName
            }
        }
        finalArray.push([name,maxValue])

    }
    

    // console.log(keys);
    // console.log(finalArray);

    let finalObj={};
    for(let index=0;index<keys.length;index++){
        finalObj[keys[index]] = finalArray[index];
    }
   console.log(finalObj);
   const jsonOutput = path.join(__dirname,'../public/output/playerOfTheMatch.json')
   fs.writeFile(jsonOutput,JSON.stringify(finalObj),(error)=>{
    if(error){
    console.log(error);
    }
})
    
})
}

module.exports = playerofTheMatch;