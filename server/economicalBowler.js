const csv = require("csv-parser");
const fs = require("fs");
const path=require("path");

function economicalBowler(){
const matchesData = [];
matchpath = path.join(__dirname,"../data/matches.csv")
fs.createReadStream(matchpath)
.pipe(csv())

.on("data",(data)=>{
    matchesData.push(data)
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
    const deliveryData = [];
    const deliveryPath = path.join(__dirname,'../data/deliveries.csv')
    fs.createReadStream(deliveryPath)
    .pipe(csv())

    .on("data",(data)=>{
        deliveryData.push(data)
    })
    .on("error",(error)=>{
        console.log(error);
    })
    .on("end",()=>{
        let bowlerWithRuns ={};
        let totalDeliveries = {}
    for(let match of matchesData){
        for(let delivery of deliveryData){
            if(match.season === "2015" && match.id === delivery.match_id)
            {
                if(bowlerWithRuns[delivery.bowler]){
                    bowlerWithRuns[delivery.bowler] += Number(delivery.total_runs)
                    totalDeliveries[delivery.bowler]++;
                }
                else{
                    bowlerWithRuns[delivery.bowler] = Number(delivery.total_runs)
                    totalDeliveries[delivery.bowler] = 1;
                }
    
            }
        }
    }
    let bowlerRunsKey = Object.keys(bowlerWithRuns);
     let strikeRate = {};
    for(let key of bowlerRunsKey){
        strikeRate[key] = parseFloat((bowlerWithRuns[key]/totalDeliveries[key])*6);
    }
    console.log(strikeRate);
    let finalStrikeRate = Object.keys(strikeRate).sort((a,b)=>{
        return  strikeRate[a] - strikeRate[b];
    }).slice(0,10);
    // console.log(finalStrikeRate);
    // let resultObject={};
    // for(let key of finalStrikeRate){
    //     resultObject[key]=strikeRate[key];
    // }
    // console.log(finalStrikeRate);
    jsonOutput = path.join(__dirname,"../public/output/economicalBowler.json")
    fs.writeFile(jsonOutput,JSON.stringify(finalStrikeRate),(error)=>{
        if(error){
            console.log(error);
        }
    })
    })
})
}

module.exports = economicalBowler;
economicalBowler()