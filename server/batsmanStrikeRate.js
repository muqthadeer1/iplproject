// importing csv and fs module
const csv = require("csv-parser");

const fs = require("fs");

const path =require("path")

function batsmanStrikeRate(){
//create empty array to store data in Array
let matchesData = [];
const matchPath =path.join(__dirname,'../data/matches.csv')

// create  a fs module to Read the file
const readStream = fs
  .createReadStream(matchPath)
  .pipe(csv())
  .on("data", (data) => {
    matchesData.push(data);
  })
  .on("error", (error) => {
    console.log(error);
  })
  .on("end", () => {
    // create  a empty array to store the delivery data in it
    const deliveryPath = path.join(__dirname,'../data/deliveries.csv')

        let keysOfSeason = {};
        for (let match of matchesData) {
          if (keysOfSeason[match.season]) {
            keysOfSeason[match.season].push(match.id);
          } else {
            keysOfSeason[match.season] = [match.id];
          }
        }

        let deliveryData = [];

        // create fs module to read the file
        fs.createReadStream(deliveryPath)
          .pipe(csv())

          .on("data", (data) => {
            deliveryData.push(data);
          })
          .on("error", (error) => {
            console.log(error);
          })
          .on("end", () => {
            let keysofmatchdata = Object.keys(keysOfSeason);
            let someObj = {};
            for (let key of keysofmatchdata) {
              for (let delivery of deliveryData) {
                if (someObj[key]) {
                  if (keysOfSeason[key].includes(delivery.match_id)) {
                    let batsman = delivery.batsman;
                    if (someObj[key][batsman]) {
                      someObj[key][batsman].runs += parseInt(
                        delivery.total_runs
                      );
                      someObj[key][batsman].balls += 1;
                    } else {
                      someObj[key][batsman] = {
                        runs: parseInt(delivery.total_runs),
                        balls: 1,
                      };
                    }
                  }
                } else {
                  // initialise as empty object
                  someObj[key] = {};
                }
              }
            }
            const finalStrikeRate = {};
            for (let yearKey in someObj) {
              for (let subKey in someObj[yearKey]) {
                // console.log(someObj[yearKey][subKey]["runs"])
                if (finalStrikeRate[yearKey]) {
                  finalStrikeRate[yearKey][subKey] =
                    (someObj[yearKey][subKey]["runs"] /
                      someObj[yearKey][subKey]["balls"]) *
                    100;
                } else {
                  finalStrikeRate[yearKey] = {};
                }
              }
            }
            console.log(finalStrikeRate);
            // create a fs module to store the output as json file
            const jsonOutput = path.join(__dirname,"../public/output/batsmanStrikeRate.json")
            fs.writeFile(
              jsonOutput,
              JSON.stringify(finalStrikeRate),
              (error) => {
                if (error) {
                  console.log(error);
                }
              }
            );
          });
      
  });
}

module.exports = batsmanStrikeRate;