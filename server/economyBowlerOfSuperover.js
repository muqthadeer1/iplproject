const csv = require("csv-parser");
const fs = require("fs");
const path  = require("path");

function economyBowlerOfSuperover(){
const deliveryData = [];

const deliveryDatapath = path.join(__dirname,"../data/deliveries.csv")
const  readStream =  fs.createReadStream(deliveryDatapath)
.pipe(csv())

.on("data",(data)=>{
    deliveryData.push(data);
})
.on("error",(error)=>{
    console.log(error);
})
.on("end",()=>{
    bowlerRuns = {};
    bowlerBalls = {};

    for(let keys of deliveryData){
        if(keys.is_super_over == 1){
            if(bowlerRuns[keys.bowler]){
                bowlerRuns[keys.bowler] += Number(keys.total_runs);
                bowlerBalls[keys.bowler]++;
            }else{
                
                bowlerRuns[keys.bowler] = Number(keys.total_runs);
                bowlerBalls[keys.bowler] = 1;
            }

        }
    }
    let keyOfRuns = Object.keys(bowlerRuns);
    let keyOfBalls = Object.keys(bowlerBalls);
    const finalEconomy = {};
    for(let index = 0;index<keyOfRuns.length;index++){

        if(keyOfRuns[index]==keyOfBalls[index]){
          finalEconomy[keyOfBalls[index]] = (bowlerRuns[keyOfRuns[index]]/bowlerBalls[keyOfBalls[index]]) * 6
        }
    }
    let finalEconomyBowler = Object.keys(finalEconomy).sort((a,b)=>{
        return  finalEconomy[a] - finalEconomy[b];
    }).slice(0,1);
    console.log(finalEconomyBowler);
   
    const jsonOutput = path.join(__dirname,'../public/output/economyBowlerOfSuperover.JSON')
    fs.writeFile(jsonOutput,JSON.stringify(finalEconomyBowler),(error)=>{
        if(error){
        console.log(error);
        }
    })
   
})
}

module.exports =economyBowlerOfSuperover;